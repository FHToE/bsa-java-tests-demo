package com.example.demo.dto.mapper;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.model.ToDoEntity;
import org.junit.jupiter.api.Test;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.*;

class ToDoEntityToResponseMapperTest {

    @Test
    void whenGetNull_thenReturnNull() {
        assertEquals(null, ToDoEntityToResponseMapper.map(null));
    }

    @Test
    void whenGetEntity_thenReturnResponse() {
        //mock
        ZonedDateTime mockTime = ZonedDateTime.now(ZoneOffset.UTC);
        ToDoEntity mockEntity = new ToDoEntity(0L,"text", mockTime);
        ToDoResponse mockResult = new ToDoResponse(0L,"text", mockTime);
        //validate
        assertThat(mockResult, samePropertyValuesAs(ToDoEntityToResponseMapper.map(mockEntity)));
    }
}