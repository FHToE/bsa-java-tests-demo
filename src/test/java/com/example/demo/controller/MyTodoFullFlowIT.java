package com.example.demo.controller;

import com.example.demo.controller.Util.IntegrationTestUtil;
import com.example.demo.dto.ToDoBetweenRequestDto;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@SpringBootTest
@AutoConfigureMockMvc
public class MyTodoFullFlowIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ToDoRepository toDoRepository;

    @BeforeEach
    void seed() {
        var toDo1 = new ToDoEntity("Successful finish test stage",
                ZonedDateTime.of(2020, 5, 2, 12, 0, 0, 0, ZoneOffset.UTC));

        var toDo2 = new ToDoEntity("Successful finish homework for lecture",
                ZonedDateTime.of(2020, 5, 6, 12, 0, 0, 0, ZoneOffset.UTC));
        var toDo3 = new ToDoEntity("Successful finish homework for lecture",
                ZonedDateTime.of(2020, 5, 12, 12, 0, 0, 0, ZoneOffset.UTC));
        var toDo4 = new ToDoEntity("Successful finish homework for lecture",
                ZonedDateTime.of(2020, 5, 17, 12, 0, 0, 0, ZoneOffset.UTC));
        var toDo5 = new ToDoEntity("Hurray! Successful finish lectures selection stage",
                ZonedDateTime.of(2020, 5, 25, 12, 0, 0, 0, ZoneOffset.UTC));
        var toDo6 = new ToDoEntity("Successful finish interview selection stage",
                ZonedDateTime.of(2020, 6, 24, 12, 0, 0, 0, ZoneOffset.UTC));
        var toDo7 = new ToDoEntity("Successful finish my mini-project",
                ZonedDateTime.of(2020, 6, 24, 12, 0, 0, 0, ZoneOffset.UTC));
        var toDo8 = new ToDoEntity("Successful finish homework for lecture",
                ZonedDateTime.of(2020, 7, 7, 12, 0, 0, 0, ZoneOffset.UTC));
        var toDo9 = new ToDoEntity( "Successful finish homework for lecture",
                ZonedDateTime.of(2020, 7, 11, 12, 0, 0, 0, ZoneOffset.UTC));
        var toDo10 = new ToDoEntity("Successful finish homework for lecture");
        var toDo11 = new ToDoEntity("Successful finish lectures stage");
        var toDo12 = new ToDoEntity("Successful finish project stage");
        var toDo13 = new ToDoEntity("Become strong developer;)");
        toDoRepository.deleteAll();
        toDoRepository.saveAll(Arrays.asList(toDo1, toDo2, toDo3, toDo4, toDo5, toDo6, toDo7,
                toDo8, toDo9, toDo10, toDo11, toDo12, toDo13));
    }

    @Test
    void whenGet3OldestByValidQuery_thenReturn3Oldest() throws Exception {
        String query = "Successful finish homework for lecture";
        this.mockMvc
                .perform(get("/todos/" + query + "/oldest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].text").value(query))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].completedAt").exists());
    }

    @Test
    void whenGet3OldestByBadQuery_thenReturnResolvedException() throws Exception {
        String badQuery = "Some bad, nonexistent TODO";
        String expectedExMessage = "Can not find todo with text: " + badQuery;
        this.mockMvc
                .perform(get("/todos/" + badQuery + "/oldest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(result -> assertEquals(expectedExMessage, result.getResolvedException().getMessage()));
    }

    @Test
    void whenGetToDoWithLongestTextAndRepositoryNotEmpty_thenReturnToDoWithLongestText() throws Exception {
        this.mockMvc
                .perform(get("/todos/longest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text")
                        .value("Hurray! Successful finish lectures selection stage"));
    }

    @Test
    void whenGetToDoWithLongestTextAndRepositoryIsEmpty_thenReturnResolvedException() throws Exception {
        toDoRepository.deleteAll();
        this.mockMvc
                .perform(get("/todos/longest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(result -> assertEquals("No entities in repository",
                        result.getResolvedException().getMessage()));
    }

    @Test
    void whenDeleteInExistingRange_thenDeleteAppropriateToDos() throws Exception {
        ZonedDateTime from = ZonedDateTime.of(2020,5,6, 0,
                0,0,0, ZoneOffset.UTC);
        ZonedDateTime till = ZonedDateTime.of(2020,5,26, 0,
                0,0,0, ZoneOffset.UTC);
        ToDoBetweenRequestDto mockRequestDto = new ToDoBetweenRequestDto(from, till);

        this.mockMvc.perform(delete("/interval").contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(mockRequestDto)))
                    .andExpect(status().is(204));

        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(9)))
                .andExpect(jsonPath("$[0].text").value("Successful finish test stage"))
                .andExpect(jsonPath("$[1].text").value("Successful finish interview selection stage"))
                .andExpect(jsonPath("$[0].id").isNumber());
    }
}
