package com.example.demo.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import static org.mockito.Mockito.*;
import com.example.demo.dto.ToDoResponse;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.springframework.data.domain.Pageable;

class ToDoServiceTest {

	private ToDoRepository toDoRepository;

	private ToDoService toDoService;

	//executes before each test defined below
	@BeforeEach
	void setUp() {
		this.toDoRepository = mock(ToDoRepository.class);
		toDoService = new ToDoService(toDoRepository);
	}

	@Test
	void whenFind3OldestByText_thenReturn3OldestByText() throws ToDoNotFoundException {
		//mock
		String mockQuery = "Test";
		var toDo1 = new ToDoEntity("Test");
		toDo1.completeNow();
		var toDo2 = new ToDoEntity("Test");
		toDo2.completeNow();
		var toDo3 = new ToDoEntity("Test");
		toDo3.completeNow();
		List<ToDoEntity> testToDos = new ArrayList<>(Arrays.asList(toDo1, toDo2, toDo3));
		when(toDoRepository.find3OldestByText(anyString(), isA(Pageable.class))).thenAnswer(i->{
		String query = i.getArgument(0, String.class);
		if (query.equals(mockQuery)) {
			return testToDos.stream().map(ToDoEntityToResponseMapper::map).collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}
		});

		//call
		List<ToDoResponse> todos = toDoService.find3OldestByText(mockQuery);

		//validate
		assertEquals(todos.size(), testToDos.size());
		assertEquals(todos.get(0).text, mockQuery);
		for (int i = 0; i < todos.size(); i++) {
			assertThat(todos.get(i), samePropertyValuesAs(
					ToDoEntityToResponseMapper.map(testToDos.get(i))
			));
		}

	}
	@Test
	void whenFind3OldestByWrongText_thenReturnException() {
		//mock
		String badMockQuery = "test";
		when(toDoRepository.find3OldestByText(anyString(), isA(Pageable.class))).thenAnswer(i->{
			String query = i.getArgument(0, String.class);
			List<ToDoResponse> testToDos = new ArrayList<>();
			if (!query.equals(badMockQuery)) testToDos.add(new ToDoResponse(0L,"text",ZonedDateTime.now()));
			return testToDos;
		});
		//validate
		ToDoNotFoundException exception = assertThrows(ToDoNotFoundException.class,
				() -> toDoService.find3OldestByText(badMockQuery));
		assertThat(exception.getMessage(), containsString("Can not find todo with text: "));
	}

	@Test
	void whenDeleteByTimeInterval_thenRepositoryDeleteCalled() {
		ZonedDateTime mockTime1 = ZonedDateTime.now(ZoneOffset.UTC);
		ZonedDateTime mockTime2 = ZonedDateTime.now(ZoneOffset.UTC);
		//call
		toDoService.deleteByTimeInterval(mockTime1, mockTime2);
		//validate
		verify(toDoRepository, times(1)).deleteAllByCompletedAtBetween(mockTime1, mockTime2);
	}

}
