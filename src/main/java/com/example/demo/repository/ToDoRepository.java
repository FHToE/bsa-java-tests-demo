package com.example.demo.repository;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.model.ToDoEntity;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

public interface ToDoRepository extends JpaRepository<ToDoEntity, Long> {

    @Query("SELECT new com.example.demo.dto.ToDoResponse(td.id, td.text, td.completedAt) FROM ToDoEntity td " +
            "WHERE td.text = :text " +
            "ORDER BY td.completedAt ASC NULLS LAST, td.id")
    List<ToDoResponse> find3OldestByText(@Param("text")String text, Pageable pageable);

    @Transactional
    List<ToDoEntity> deleteAllByCompletedAtBetween (ZonedDateTime from, ZonedDateTime till);

    @Query("SELECT new com.example.demo.dto.ToDoResponse(t.id, t.text, t.completedAt) from ToDoEntity t " +
            "ORDER BY LENGTH(t.text) DESC, t.text")
    List<ToDoResponse> findToDoWithLongestText(Pageable pageable);
}