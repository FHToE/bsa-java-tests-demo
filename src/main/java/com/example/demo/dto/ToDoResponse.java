package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ToDoResponse {
	@NotNull
	public Long id;

	@NotNull
	public String text;

	public ZonedDateTime completedAt;
}