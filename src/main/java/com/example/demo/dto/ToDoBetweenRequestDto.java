package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ToDoBetweenRequestDto {
    private ZonedDateTime from;
    private ZonedDateTime till;
}
